require 'progress_bar'

bar = ProgressBar.new(1500, :bar, :elapsed)

1500.times do
  sleep 1
  bar.increment!
end
